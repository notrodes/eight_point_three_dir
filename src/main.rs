use std::fs;

use sfnutils::tracker::NameTracker;

fn main() {
    let mut tracker = NameTracker::new();
    for file_name in fs::read_dir(".").unwrap() {
        let file_name = file_name.unwrap().file_name().into_string().unwrap();
        let file_name = sfnutils::from_with_tracker(file_name, &mut tracker);
        println!("{}", file_name.short_name)
    }
}